class FoodsController < ApplicationController
  def index
    @foods = Food.all
  end

  def show
    @food = Food.find params[:id]
  end

  def new
    @food = Food.new
    @artists = Artist.all
    @museums = Museum.all
  end

  def create
    safe_painting = params.require(:food).permit(:title, :image_url, :places, :descript)
    @food = Food.create safe_painting
    redirect_to @food, notice: "Food successfully added"
  end
end
