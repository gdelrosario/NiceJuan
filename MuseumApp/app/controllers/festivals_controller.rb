class FestivalsController < ApplicationController
  def index
    @festivals = Festival.all
  end

  def show
    @festival = Festival.find params[:id]
  end

  def new
    @festival = Festival.new
    @artists = Artist.all
    @museums = Museum.all
  end

  def create
    safe_painting = params.require(:festival).permit(:title, :image_url, :place, :description)
    @festival = Festival.create safe_painting
    redirect_to @festival, notice: "Festival successfully added"
  end
end
